const Mailgun = require('mailgun-js');
const moment = require("moment-timezone");
const fs = require("fs");
const path = require("path");
const _ = require("underscore");

// go to data/cloud directory because that's where cloud code sits
const mail_template_string = fs.readFileSync( path.resolve('data', 'cloud', "store_email_template.html"), "utf8");
const mail_template = _.template(mail_template_string);

// init SETTINGS with default settings
let SETTINGS = {
	STRIPE_SK_PROD: "sk_test_Ck7YJaP6b8LeCs2Gh7NZsZvk", // Zingon example key, testing only
	STRIPE_SK_TEST: "sk_test_Ck7YJaP6b8LeCs2Gh7NZsZvk", // Zingon example key, testing only

	DEFAULT_TIMEZONE: "US/Central", // moment-timezone formatted
	TIME_FORMAT_STRING: "YYYY-MMM-DD hh:mm:ss A",

	MAIL_SUBJECT_TEMPLATE: "New Order",
	MAILGUN_KEY: "58c6f408b1ea3d024d59e4a472bfeba4-8b7bf2f1-c4e9974d",
	MAILGUN_DOMAIN: "zingon-data.com",

	PUSHNOTIFICATION_ENABLED: false,

	REWARDS_INTEGRATION: undefined,
};

let defaultTimezone = SETTINGS.DEFAULT_TIMEZONE;
let timeFormat = SETTINGS.TIME_FORMAT_STRING;

let mail_subject_template = null;
if (SETTINGS.MAIL_SUBJECT_TEMPLATE) {
	mail_subject_template = _.template(SETTINGS.MAIL_SUBJECT_TEMPLATE);
}

let stripeProd = require("stripe")(SETTINGS.STRIPE_SK_PROD);
let stripeTest = require("stripe")(SETTINGS.STRIPE_SK_TEST);
let stripe = stripeProd;


// use like this: require('ro-cloud-code')({STRIPE_SK_PROD: '< new stripe key >'})
module.exports = settings => {
	// apply settings that have changed from defaults
	SETTINGS = _.extend(SETTINGS, settings);

	defaultTimezone = SETTINGS.DEFAULT_TIMEZONE;
	timeFormat = SETTINGS.TIME_FORMAT_STRING;

	mail_subject_template = null;
	if (SETTINGS.MAIL_SUBJECT_TEMPLATE) {
		mail_subject_template = _.template(SETTINGS.MAIL_SUBJECT_TEMPLATE);
	}

	stripeProd = require("stripe")(SETTINGS.STRIPE_SK_PROD);
	stripeTest = require("stripe")(SETTINGS.STRIPE_SK_TEST);
	stripe = stripeProd;

};


Parse.Cloud.afterSave("Order", function(request) {
	let order = request.object;
	if(order.get('status') === 'PAID' && request.original.get('status') !== 'PAID') {
    	sendOrderEmail(order);



		// allow testing of rewards integration
		if ((order.get('pickupName') === 'Kai zing')) {
			if (SETTINGS.REWARDS_INTEGRATION) {
				let store = order.get('store');
				store.fetch().then( store => {
					debugLog('sending order to rewards integration');
					SETTINGS.REWARDS_INTEGRATION.recordOrderOnRewards(order).then( response => {
						debugLog({response: response});
					});
				});
			}
		}


    }
});

function sendOrderEmail(orderObject) {
	// debugLog("MAILGUN process started");

	// reload the object to fetch everything we need in one query
	const query = new Parse.Query(Parse.Object.extend("Order"));
    query.equalTo("objectId", orderObject.id);
	query.include('items.menuItem');
	query.include('items.options');
	query.include('items.options.menuItemOption');
	query.include('items.options.selectedValues');
	query.include('store.orderEmailRecipients');

	// debugLog("MAILGUN loading order...");
    query.find({
		success: function(results) {
			const order = results[0];
			// debugLog("...MAILGUN order loaded");
			//We pass the api_key and domain to the wrapper, or it won't be able to identify + send emails
			const mailgun = new Mailgun({
				apiKey: SETTINGS.MAILGUN_KEY,
				domain: SETTINGS.MAILGUN_DOMAIN
			});

			let subjectLine = constructOrderEmailSubject(order);
			let recipients = [];

			const store = order.get('store');
			let timezone = defaultTimezone;
			if(!store) {
				errorLog("ORDER EMAIL error: no store set for order");
				subjectLine += ' - NO STORE SET';
				recipients = ['kai+orderingERROR@zingon.com'];
			} else {
				recipients = store.get('orderEmailRecipients');
				timezone = store.get(timezone);
				if(!recipients || !recipients.length) {
					errorLog("ORDER EMAIL error: no order email recipients");
					subjectLine += ' - NO ORDER EMAIL RECIPIENTS SET';
					recipients = ['kai+orderingERROR@zingon.com'];
				}
			}

			if (!timezone) { timezone = defaultTimezone; }

			// Set the orderedAt time
			order.set('orderedAt', new Date());
			order.save().then(
				success => {
					// debugLog("MAILGUN constructOrderEmailBody...");
					const b = constructOrderEmailBody(order, timezone);
					// debugLog("...MAILGUN constructOrderEmailBody finished");

					const data = {
						//Specify email data
						from: 'ordering@zingmobileapps.com',
						//The email to contact
						to: recipients,
						bcc: ['kai@zingon.com'],
						//Subject and text data
						subject: subjectLine,
						html: b
					};

					//Invokes the method to send emails given the above data with the helper library
					mailgun.messages().send(data, function (err, body) {
						//If there is an error, render the error page
						if (err) {
							errorLog("MAILGUN got an error: ", err);
						}
						else {
							order.set('status', 'SENT');
							order.save();
						}
					});
				},
				error => { errorLog('Save orderedAt error:' + error); }
			);


		},
		error: function(error) {
			//error
			errorLog("MAILGUN error: " + error);
		}
    });
}

function constructOrderEmailBody(order, timezone) {

	let ret = 'UNABLE TO CONSTRUCT ORDER EMAIL FOR ORDER ' + order.id;
	try {
		ret = mail_template({
			order: order,
			ro_getTimezoneTime: ro_getTimezoneTime,
			timezone: timezone
		  });
	} catch(error) {
		errorLog('ERROR applying email template: ' + error);
	}

	return ret;
}


function constructOrderEmailSubject(order) {

	let ret = 'New Order ' + order.id;

	if (mail_subject_template) {
		try {
			ret = mail_subject_template({
				order: order
			});
		} catch(error) {
			errorLog('ERROR applying email subject template: ' +  error);
		}
	}

	return ret;
}


// Convert the handed time in the handed timezone and return it in the format '2019-02-06 11:41:38 AM'
function ro_getTimezoneTime(time, timeZone) {

	let timeFormatString = timeFormat;
	if (!timeFormatString) { timeFormatString = 'M/D/YY h:mm:ss A'; }

	if (time && timeZone) {
		return moment.tz(time, timeZone).format(timeFormatString);
	} else {
		return '';
	}
}


const API_Version = '1.5';
const MESSAGE_WRONG_API_VERSION = 'Your app version is too old. Please update or restart the app!';
const max_totalprice_per_order = 100;
const order_timespan = 60; // Timespan in minutes
const max_orders_per_timespan = 25;

//ro_stripeKey
Parse.Cloud.define("ro_stripeKey", function(request, response) {

	const api_version = request.params.api_version;
	const customerId = request.params.customerId;
	const deviceKey = request.params.deviceId;
	const pickUpName = request.params.pickUpName;

	let stripe = stripeProd;
	if (request.params.test) {
		stripe = stripeTest;
	}

	if (!api_version) {
		return response.error('missing stripe API version');
	}


	const error = err => {
		return response.error(err);
	};

	const success = customer => {
		stripe.ephemeralKeys
			.create({customer: customer.id}, {stripe_version: api_version})
			.then(key => {
				return response.success(key);
			})
			.catch(error);
	};

	if (customerId) {
      stripe.customers
        .retrieve(customerId)
        .then(success)
        .catch(error);
    } else {
      stripe.customers
        .create({
          description: pickUpName || "customer",
          metadata: { device_key: deviceKey }
        })
        .then(success)
        .catch(error);
    }
 });

//ro_stripeCharge
Parse.Cloud.define("ro_stripeCharge", function(request, response) {

	const query = new Parse.Query('Order');
	query.equalTo('objectId', request.params.metadata.orderId);
	query.find({useMasterKey: true}).then(
		find_order_success => {

		const order = find_order_success[0];

			if (order.get('stripePaid')) {
				console.error('order already paid: ' + order);
				// similar to Parse's 137 "DuplicateValue"
				response.error(-137, 'DuplicatePayment');
				return;
			}

		let stripe = stripeProd;
		if (request.params.test) {
			stripe = stripeTest;
		}

		// Create the charge on Stripe's servers - this will charge the user's card
		// debugLog("create charge for " + JSON.stringify(request.params));
		let params = {
			"amount": request.params['amount'], // this number should be in cents
			"currency": "usd",
			"source": request.params['source'],
			"description": request.params["description"],
			"metadata": request.params["metadata"] || {}
		};

		if (request.params['capture'] != null) {
			params['capture'] = request.params['capture'];
		}

		if (request.params['customer_id']) {
			params['customer'] = request.params['customer_id'];
		}

		stripe.charges.create(params).then( charge => {
			// debugLog("created charge");

			// Charge was created - save the status (PAID), the stripeChargeId and the stripePaid flag to the order
			if (!request.params.metadata.orderId) {
				errorLog('No order id');
				return response.error('No Order id');
			}

				order.set('status', 'PAID');

				if (request.params.capture) {
				    order.set('stripePaid', true);
                    if (SETTINGS.REWARDS_INTEGRATION) {
                        SETTINGS.REWARDS_INTEGRATION.recordOrderOnRewards(order);
                    }
				} else {
				    order.set('stripePaid', false);
				}

				if (charge.id && charge.id.length > 0) { order.set('stripeChargeId', charge.id); }

				order.save().then(
					save_order_success => {
						// debugLog('ro_stripeCharge: Order saved');
						return response.success(charge);
					},
					save_order_error => {
						errorLog('Order could not be saved');
						return response.error(save_order_error);
					}
				);
			},
			find_order_error => {
				errorLog('Order could not be found');
				return response.error(find_order_error);
			}
		);


		//return response.success(charge);
	}).catch( err => {
		errorLog("error loading order for charge");
		errorLog(err);
		return response.error(err);
	});
});

Parse.Cloud.define("ro_getAllCategoriesFromStore", function(request, response) {

	// Check API version. Leave with error if not equal.
	if (ro_checkAPIVersion(request.params.api_version) === false) {
		response.error(MESSAGE_WRONG_API_VERSION);
		return;
	}

	if (!request.params.store_id) { reponse.error('Data could not be loaded: no store submitted.'); return; }

	const query = new Parse.Query('Store');
	query.equalTo('objectId', request.params.store_id);

	query.include('categories');
	query.include('categories.limitations');
    query.include('categories.menus');
	query.include('categories.menus.limitations');
    query.include('categories.menus.menuItems');
    query.include('categories.menus.menuItems.options');
    query.include('categories.menus.menuItems.options.values');
    query.include('categories.menus.menuItems.discounts');
    
    query.find({useMasterKey: true}).then(
		(success) => {
			if (success && (success.length > 0)) {
				const categories =  success[0].get('categories');
				response.success(categories);
			} else {
				response.error('Data could not be loaded: no store found.');
			}
		},
		(error) => {
			response.error('Data could not be loaded.');
		}
    );
});


Parse.Cloud.define("ro_getAllCategories", function(request, response) {

	// Check API version. Leave with error if not equal.
	if (ro_checkAPIVersion(request.params.api_version) === false) {
		response.error(MESSAGE_WRONG_API_VERSION);
		return;
	}

    const query = new Parse.Query('Category');
	query.include('limitations');
    query.include('menus');
	query.include('menus.limitations');
    query.include('menus.menuItems');
    query.include('menus.menuItems.options');
    query.include('menus.menuItems.options.values');
	query.include('categories.menus.menuItems.discounts');
	query.include('limitations');

    query.ascending('order');
    query.equalTo('active', true);

    query.find({useMasterKey: true}).then(
		(success) => { response.success(success); },
		(error) => { response.error('Data could not be loaded.'); }
    );
});


Parse.Cloud.define("ro_getOrderHistory", function(request, response) {

	// Check API version. Leave with error if not equal.
	if (ro_checkAPIVersion(request.params.api_version) === false) {
		response.error(MESSAGE_WRONG_API_VERSION);
		return;
	}

	// Check if a device id submitted
	if (!request.params.deviceId) { response.error('The order history could not be loaded: No device ID submitted.'); return; }

	const limit = (request.params.limit) ? request.params.limit : 20;

    const query = new Parse.Query('Order');
    query.include([
      'items',
      'items.menuItem',
      'items.menuItem.options',
      'items.options',
      'items.options.menuItemOption',
      'items.options.selectedValues',
      'store',
      'store.limitations'
    ]);
    query.descending('updatedAt');
    query.limit(limit);
	query.equalTo('deviceId', request.params.deviceId);

	query.find({useMasterKey: true}).then(
		(success) => { response.success(success); },
		(error) => { response.error('The order history could not be loaded.'); }
    );
});


Parse.Cloud.define("ro_getCurrentOrders", function(request, response) {

	// Check API version. Leave with error if not equal.
	if (ro_checkAPIVersion(request.params.api_version) === false) {
		response.error(MESSAGE_WRONG_API_VERSION);
		return;
	}

	// Check if a device id submitted
	if (!request.params.deviceId) { response.error('The current order list could not be loaded: No device ID submitted.'); return; }

	const nowMinus30Minutes = new Date();
	nowMinus30Minutes.setMinutes(nowMinus30Minutes.getMinutes() - 30);

	const query = new Parse.Query('Order');
	query.containedIn('status', ['SENT', 'PAID', 'ACCEPTED', 'REJECTED', 'COMPLETED', 'PICKEDUP']);
	query.greaterThanOrEqualTo('pickupTime', nowMinus30Minutes);
	query.equalTo('deviceId', request.params.deviceId);

	query.find().then(
		success => {
			response.success(success);
		},
		error => { response.error('The current order list could not be loaded.'); }
	);
});


Parse.Cloud.define("ro_getOrderItem", function(request, response) {

	// Check API version. Leave with error if not equal.
	if (ro_checkAPIVersion(request.params.api_version) === false) {
		response.error(MESSAGE_WRONG_API_VERSION);
		return;
	}

	if (!request.params.id) { response.error('The order item could not be loaded: no item id submitted.'); return; }

	const query = new Parse.Query('OrderItem');
	query.equalTo('objectId', request.params.id);
	query.include('options');
	query.include('menuItem');
	query.include('menuItem.options');
	query.include('menuItem.options.values');
	query.include('menuItemOption');
	query.include('menuItemOption.values');
	query.include('options.selectedValues');

	query.find({useMasterKey: true}).then(
		(success) => { response.success(success); },
		(error) => { response.error('The order item could not be loaded.'); }
    );
});


// We don't use the cloud code for saving an order
Parse.Cloud.define("ro_saveOrder", function(request, response) {

	// Check API version. Leave with error if not equal.
	if (ro_checkAPIVersion(request.params.api_version) === false) {
		response.error(MESSAGE_WRONG_API_VERSION);
		return;
	}

	// Check if a device id submitted
	if (!request.params.device_id) { response.error('Sorry, we couldn\'t save your order: No device ID submitted.'); return; }

	// Check if an order submitted
	if (!request.params || !request.params.order) { response.error('Sorry, we couldn\'t save your order: No order submitted.'); return; }

	// Check if the price of this order is too high
	if (request.params.order.total > max_totalprice_per_order) { response.error('Sorry, the total price of your order is too high. The limit for one order is: $' + max_totalprice_per_order.toString()); return; }

    const Order = Parse.Object.extend('Order');
    const OrderItem = Parse.Object.extend('OrderItem');
    const OrderItemOption = Parse.Object.extend('OrderItemOption');
    const MenuItem = Parse.Object.extend('MenuItem');
    const MenuItemOption = Parse.Object.extend('MenuItemOption');
    const MenuItemOptionValue = Parse.Object.extend('MenuItemOptionValue');

	// Create order
    const newOrder = new Order();

    newOrder.set('total', request.params.order.total);
    newOrder.set('pickupName', request.params.order.pickupName);
    newOrder.set('tax', request.params.order.tax);
	newOrder.set('deviceId', request.params.device_id);

	// Create items
    request.params.order.items.forEach( item => {
        const newItem = new OrderItem();
        newOrder.add('items', newItem);
        newItem.set('menuItem', MenuItem.createWithoutData(item.menuItem));
        newItem.set('quantity', item.quantity);

		// Create options
        item.options.forEach( option => {
            const newOption = new OrderItemOption();
            newItem.add('options', newOption);
            newOption.set('menuItemOption', MenuItemOption.createWithoutData(option.menuItemOption));

            option.selectedValues.forEach( value => {
              newOption.add('selectedValues', MenuItemOptionValue.createWithoutData(value));
            });
        });
    });

	// Save items (incl. options)
    Parse.Object.saveAll(newOrder.get('items')).then(
        successSaveAll => {
		  // Save order
          newOrder.save().then(
              successSave => {
                  response.success(successSave);
              },
              errorSave => {
                  response.error('Sorry, we couldn\'t save your order! Please try again.');
              });
        },
        errorSaveAll => {
            response.error('Sorry, we couldn\'t save your order! Please try again.');
        });
});


Parse.Cloud.define("ro_getStores", function(request, response) {

	// Check API version. Leave with error if not equal.
	if (ro_checkAPIVersion(request.params.api_version) === false) {
		response.error(MESSAGE_WRONG_API_VERSION);
		return;
	}

	const query = new Parse.Query('Store');
	query.include('categories');
	query.include('categories.limitations');
	query.include('limitations');
	query.notEqualTo('categories', null);

	query.find({useMasterKey: true}).then(
		(success) => { response.success(success); },
		(error) => { response.error('The store list could not be loaded.'); }
    );
});


Parse.Cloud.define("ro_getStore", function(request, response) {

	// Check API version. Leave with error if not equal.
	if (ro_checkAPIVersion(request.params.api_version) === false) {
		response.error(MESSAGE_WRONG_API_VERSION);
		return;
	}

	if (!request.params || !request.params.store_id) { response.error('Sorry, we couldn\'t load the current store: No store ID submitted.'); return; }

	const query = new Parse.Query('Store');

	query.equalTo('objectId', request.params.store_id);
	query.include('limitations');
	query.find({useMasterKey: true}).then(
		(success) => { response.success(success); },
		(error) => { response.error('The current store could not be loaded.'); }
    );
});

/**
 * if the order is valid, this returns a list of discounts
 * that can be applied to this order
 */
Parse.Cloud.define("ro_validateOrder", function(request, response) {

	// Check API version. Leave with error if not equal.
	if (ro_checkAPIVersion(request.params.api_version) === false) {
		response.error(MESSAGE_WRONG_API_VERSION);
		return;
	}

	// Check if a device id submitted
	if (!request.params.device_id) { response.error('Sorry, we couldn\'t accept your order: No device ID submitted.'); return; }

	// Check if an order id submitted
	if (!request.params.order_id) { response.error('Sorry, we couldn\'t accept your order: No order ID submitted.'); return; }

	// Find order
	const query = new Parse.Query('Order');
	query.include([
		'store',
		'store.limitations'
	]);
	query.equalTo('objectId', request.params.order_id);
	query.find({useMasterKey: true}).then(
		(result) => {

			if ((result || []).length === 0) {
				 response.error('Sorry, we couldn\'t accept your order: Current order not found.');
				 return;
			}

			const currentOrder = result[0];

			// Check total price
			if (currentOrder.get('total') > max_totalprice_per_order) {
				ro_setOrderStatus(currentOrder, 'REJECTED');
				response.error('Sorry, the total price of your order is too high. The limit for one order is: $' + max_totalprice_per_order.toString() + '.');
				return;
			}

			// Check number of orders in timespan
			ro_getNumberOrdersInTimespan(order_timespan, request.params.device_id).then(
				(countOrders) => {
					if (countOrders > max_orders_per_timespan) {
						ro_setOrderStatus(currentOrder, 'REJECTED');
						response.error('Sorry, we couldn\'t accept your order: Too many orders in the last ' + order_timespan.toString() + ' minutes.');

					} else {

						// Check if the store will be closed at pickup timeout
						const resultIsStoreClosed = validate_isStoreClosed(currentOrder);
						// Store will be closed -> reject order
						if (resultIsStoreClosed) {
							ro_setOrderStatus(currentOrder, 'REJECTED');
							return response.error('Sorry, we couldn\'t accept your order: ' + resultIsStoreClosed);
						}
						// Calculate and set pickup time
						const pickupTime = new Date();
						const pickupTimeMinutes = currentOrder.get('pickupTimeMinutes');

						if (pickupTimeMinutes) { pickupTime.setMinutes(pickupTime.getMinutes() + pickupTimeMinutes); }

						currentOrder.set('pickupTime', pickupTime);

						// Don't use ro_setOrderStatus, because we need to return an error message in the case something went wrong
						currentOrder.set('status', 'VALIDATED');
						currentOrder.save().then(
							 savedOrder => {
							 	// attach available discounts to this order
    							 const query = new Parse.Query('OrderDiscount');
								query.equalTo('active', true);
								query.find({useMasterKey: true}).then(
									discounts => {
        								return response.success({'discounts': discounts});
									}, error => {
										response.error('Something went wrong when trying to load available discounts. Please try again later.');
									}
								);
							 },
							 error => { response.error('Sorry, we couldn\'t validate your order.'); }
						);

					}

				},
				(error) => { ro_setOrderStatus(currentOrder, 'REJECTED'); response.error('Sorry, we couldn\'t accept your order: Unable to find previous order count.');  }
			);

		},
		(error) => { response.error('Sorry, we couldn\'t accept your order: Current order not found.');  }
	);

});


// Store-Backend: Accept order
Parse.Cloud.define("ro_acceptOrder", function(request, response) {

	// Do we need an API-Check?

	// Check if a order id submitted
	if (!request.params.order_id) { response.error('Sorry, we couldn\'t accept the order: No order ID submitted.'); return; }

	const query = new Parse.Query('Order');
	query.equalTo('objectId', request.params.order_id);
	query.find({useMasterKey: true}).then(
		result => {

			if ((result || []).length === 0) {
				 response.error('Sorry, we couldn\'t accept the order: Order not found.');
				 return;
			}

			const order = result[0];
			const expirationTime = (order.get('pickupTimeMinutes') + 30) * 60; // in seconds (!)

			order.set('status', 'ACCEPTED');
			order.save().then(
				success => {
					// Capture charge
					stripeChargePayment(order).then(
						success_charge => {
							// Charge was captured
							order.set('stripePaid', true);
							order.save();

							// Send push notification
							sendOrderStatusPush(order.id, expirationTime, order.get('deviceId'), true);

							if (SETTINGS.REWARDS_INTEGRATION) {
								SETTINGS.REWARDS_INTEGRATION.recordOrderOnRewards(order);
							}

							response.success(success);

						},
						error_charge => {
							// Charge couldn't be captured
							order.set('status', 'REJECTED');
							order.save().then(
								success_reject => { sendOrderStatusPush(order.id, expirationTime, order.get('deviceId'), false); response.error('Sorry, charge could not be settled/captured'); },
								error_reject => { response.error('Sorry, charge could not be settled/captured'); }
							);

						}
					);
				},
				error => { response.error('Sorry, we couldn\'t accept the order: Order could not be saved.');  }
			);
		},
		error => { response.error('Sorry, we couldn\'t accept the order: Order not found.');  }
	);

});


// Store-Backend: Reject order
Parse.Cloud.define("ro_rejectOrder", function(request, response) {

	// Do we need an API-Check?

	// Check if a order id submitted
	if (!request.params.order_id) { response.error('Sorry, we couldn\'t reject the order: No order ID submitted.'); return; }

	const query = new Parse.Query('Order');
	query.equalTo('objectId', request.params.order_id);
	query.find({useMasterKey: true}).then(
		result => {

			if ((result || []).length === 0) {
				 response.error('Sorry, we couldn\'t reject the order: Order not found.');
				 return;
			}

			const order = result[0];
			const expirationTime = (order.get('pickupTimeMinutes') + 30) * 60; // in seconds (!)

			order.set('status', 'REJECTED');
			order.save().then(
				success => {
					/* //TODO: reject payment
					// send push notification -> order rejected
					sendOrderStatusPush(order.id, order.get('deviceId'), false);
					response.success(success);
					return; */

					// Refund charge
					stripeRefundPayment(order).then(
						success_refund => {
							// Send push notification
							sendOrderStatusPush(order.id, expirationTime, order.get('deviceId'), false);
							response.success(success);

						},
						error_refund => {
							// Send push notification
							sendOrderStatusPush(order.id, expirationTime, order.get('deviceId'), false);
							// Charge couldn't be captured
							// TODO: Handle the situation that the order has the status accepted - but the charge could not be captured
							response.error('Sorry, charge could not be refunded');
						}
					);

				},
				error => { response.error('Sorry, we couldn\'t reject the order: Order could not be saved.');  }
			);
		},
		error => { response.error('Sorry, we couldn\'t reject the order: Order not found.');  }
	);

});


// For better testing: Send a order status push notification
Parse.Cloud.define("ro_sendOrderStatusPush", function(request, response) {

	// Check if all parameter submitted
	if (!request.params.order_id) { response.error('Missing parameter: order_id'); return; }
	if (!request.params.expiration_time) { response.error('Missing parameter: expiration_time'); return; }
	if (!request.params.device_id) { response.error('Missing parameter: device_id'); return; }
	if (!request.params.accept_status) { response.error('Missing parameter: accept_status'); return; }

	sendOrderStatusPush(request.params.order_id, request.params.expiration_time, request.params.device_id, request.params.accept_status);
	response.success('Push notification has been sent');
});


// Return the default settings
Parse.Cloud.define("ro_defaultSettings", function(request, response) {
	try {
		const resultSettings = {
			defaultTimezone: SETTINGS.DEFAULT_TIMEZONE
		}
		response.success(resultSettings);
	} catch (err) {
		response.error(err.message);
	}
});





// Try to capture an authorized charge. Return value is Parse.Promise<boolean>.
function stripeChargePayment(order) {
	const promise = new Parse.Promise();

	if (!order) {
		promise.reject(false);
	} else {
		const stripeChargeId = order.get('stripeChargeId');
		if (!stripeChargeId) {
			promise.reject(false);
		} else {
			stripe.charges.capture(stripeChargeId, function(err, charge) {
				if (err) { promise.reject(false); } else { promise.resolve(true); }
			});
		}
	}

	return promise;
}


// Release an authorized charge by refunding it
function stripeRefundPayment(order) {
	const promise = new Parse.Promise();

	if (!order) {
		promise.reject(false);
	} else {
		const stripeChargeId = order.get('stripeChargeId');
		if (!stripeChargeId) {
			promise.reject(false);
		} else {
			stripe.refunds.create({
				charge: stripeChargeId
			}, function(err, refund) {
				if (err) { promise.reject(false); } else { promise.resolve(true); }
			});
		}
	}

	return promise;
}


function sendOrderStatusPush(orderId, expirationTime, deviceKey, accepted) {
	// Leave, if push notification is disabled (in config.json)
	if (!SETTINGS.PUSHNOTIFICATION_ENABLED) { return; }

    // debugLog('attempting to send push to ' + deviceKey + ' order: ' + orderId + ' accepted: ' + accepted);

	if (!deviceKey) {
		errorLog('unable to send push to null device key');
		return;
	}

	const query = new Parse.Query(Parse.Installation);
	query.exists('deviceKey');
	query.equalTo('deviceKey', deviceKey);

	const data = {
		'type': 'ordering',
		'order': orderId,
		'accepted': accepted
	};
	if (accepted) {
		data.alert = "Your order will be ready soon!";
	} else {
		data.alert = "One or more items are out of stock, please see an attendant for more details.";
	}

	Parse.Push.send({
	  'where': query,
	  'expiration_interval': expirationTime,
	  'data': data
	}, { useMasterKey: true })
	.then(function() {
	  // Push was successful
	  debugLog('push sent to (' + deviceKey + '): ' + data.alert);
	}, function(error) {
	  // Handle error
	  errorLog('unable to send push to customer device (' + deviceKey + '): ' + error);
	});
}

// Check the handed api version
function ro_checkAPIVersion(version) {
	return API_Version === version;
}


// Save the status
function ro_setOrderStatus(order, orderStatus) {
	order.set('status', orderStatus);
    order.save();
}


// Return the number of orders in the handed "timespan"
function ro_getNumberOrdersInTimespan(timeSpan /* in minutes */, deviceId) {

	const end = new Date();
	const start = new Date(end);
	start.setMinutes(end.getMinutes() - timeSpan);

	const query = new Parse.Query('Order');

	query.equalTo('deviceId', deviceId);
	query.greaterThanOrEqualTo('createdAt', start);
	query.lessThanOrEqualTo('createdAt', end);

	const promise = new Parse.Promise();

	query.count({useMasterKey: true}).then(
		(success) => { promise.resolve(success); },
		(error) => { promise.reject(error); }
    );

	return promise;
}


// Create the tables and add columns for remote ordering
Parse.Cloud.define("ro_initOrderingDB", function(request, response) {

	// MenuItemOptionValue
	const MenuItemOptionValue = Parse.Object.extend('MenuItemOptionValue');
	const menuItemOptionValue = new MenuItemOptionValue();
	menuItemOptionValue.set('name', 'Sample Option Value 1');
	menuItemOptionValue.set('price', 0.5);
	menuItemOptionValue.set('taxable', true);
	menuItemOptionValue.set('selectedByDefault', true);

	// MenuItemOption
	const MenuItemOption = Parse.Object.extend('MenuItemOption');
	const menuItemOption = new MenuItemOption();
	menuItemOption.set('name', 'Sample Option');
	menuItemOption.add('values', menuItemOptionValue);
	menuItemOption.set('numValuesMax', 1);
	menuItemOption.set('numValuesMin', 2);
	menuItemOption.set('multiSelectEnabled', true);
	menuItemOption.set('required', true);

	// ItemDiscount
	const ItemDiscount = Parse.Object.extend('ItemDiscount');
	const itemDiscount = new ItemDiscount();
	itemDiscount.set('title', 'Sample Discount');
	itemDiscount.set('active', true);
	itemDiscount.set('description', 'Sample discount. Please delete me.');
	itemDiscount.set('value', 0.1);
	itemDiscount.set('ribbonText', 'Sample 10%');

	// MenuItem
	const MenuItem = Parse.Object.extend('MenuItem');
	const menuItem = new MenuItem();
	menuItem.set('name', 'Sample Item');
	menuItem.set('description', 'Only a sample item. Please delete me.');
	menuItem.set('sku', 'XXXXXXXX');
	menuItem.set('price', 1.99);
	menuItem.set('taxable', true);
	menuItem.add('discounts', itemDiscount);
	menuItem.add('options', menuItemOption);

	// Menu
	const Menu = Parse.Object.extend('Menu');
	const menu = new Menu();
	menu.set('name', 'Sample menu');
	menu.add('menuItems', menuItem);

	// Category
	const Category = Parse.Object.extend('Category');
	const category = new Category();
	category.set('name', 'Sample Category');
	category.set('active', true);
	category.set('useAsFilter', true);
	category.set('order', 1);
	category.add('menus', menu);

  // Limitation
	const Limitation = Parse.Object.extend('Limitation');
	const limitation = new Limitation();
	limitation.set('type', 'openinghours');
	limitation.set('conditions',
		[
			{ timespans: [ { start: '00:00', end: '24:00' } ]},
			{ timespans: [ { start: '00:00', end: '24:00' } ]},
			{ timespans: [ { start: '00:00', end: '24:00' } ]},
			{ timespans: [ { start: '00:00', end: '24:00' } ]},
			{ timespans: [ { start: '00:00', end: '24:00' } ]},
			{ timespans: [ { start: '00:00', end: '24:00' } ]},
			{ timespans: [ { start: '00:00', end: '24:00' } ]}
		]
	);

	// Get Image
	getZingonImage().then(
		successImage => {
			// response.success(successImage.url());
			menuItem.set('image', successImage);
			menu.set('image', successImage);
			category.set('image', successImage);
			category.set('storeListImage', successImage);
			// Save Parse objects
			Parse.Object.saveAll([menuItemOptionValue, menuItemOption, itemDiscount, menuItem, menu, category, limitation]).then(
				success => { response.success('success'); },
				error => { response.error(error.message); }
			);
		},
		errorImage => { response.error('Error loading image: ' +  errorImage); }
	);

});

// Get the Zingon logo from the homepage via Parse.Cloud.httpRequest and return is as a promise
function getZingonImage() {

	const promise = new Parse.Promise();

	Parse.Cloud.httpRequest({
  	url: 'https://images.unsplash.com/photo-1466019251091-1933ce15ba04?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&h=200&fit=crop&ixid=eyJhcHBfaWQiOjF9',
		gzip:true
	}).then(function(httpResponse) {
		const data = Array.from(Buffer.from(httpResponse.body, 'binary'));
    const contentType = httpResponse.headers['content-type'];
    const file = new Parse.File('logo', data, contentType);
    file.save({useMasterKey: true}).then(
			successFile => {
				promise.resolve(successFile);
			}, errorFile => { promise.reject(errorFile); });
	}, function(httpResponse) {
		promise.reject(httpResponse.status);
	});

	return promise;
}

// Check if the store in the handed order will be closed at pickup time. Returns a message text if the store will be closed. Otherwise null.
function validate_isStoreClosed(order) {
	if ((!order) && (!order.get('store'))) {
		return 'No current store found!';
	} // TODO: check message
	// Find the opening hours limitation (if there is one)
	const openinghoursLimitation = (order.get('store').get('limitations') || []).find(x => x.get('type') === 'openinghours');
	if (!openinghoursLimitation) {
		return 'Store is closed or will be closing soon [1]!';
	} // TODO: check message | No opening hours limitation = mobile ordering coming soon (and store is closed)
	// We need a condition set for each day (7)
	const conditions = openinghoursLimitation.get('conditions');
	if ((conditions || []).length !== 7) {
		return null;
	} // Handle like store is not closed
	// Get the pickup time
	const currentTime = moment().tz(defaultTimezone);
	const pickupTime = currentTime.add(order.get('pickupTimeMinutes'), 'minutes');
	// Get the condition set of the weekday
	const dayOfWeek = pickupTime.isoWeekday() - 1;
	const conditionsOfDay = conditions[dayOfWeek];
	// Have the the condition set a timespans property
	if (!conditionsOfDay.timespans) {
		return null;
	} // Handle like store is not closed
	// Check if the store will be closed at pickup time
	let isClosed = true;
	for (const timespan of (conditionsOfDay.timespans || [])) {
		if (!timespan.start || !timespan.end) {
			continue;
		} // no start or end time - continue with next timespan
		// We need the date of the pickup time (combine the date of the pickup time with time from the current timespan)
		const startTime = moment.tz(pickupTime.format('MM-DD-YYYY') + timespan.start, 'MM-DD-YYYY HH:mm', defaultTimezone);
		const endTime = moment.tz(pickupTime.format('MM-DD-YYYY') + timespan.end, 'MM-DD-YYYY HH:mm', defaultTimezone);
		if (!startTime.isValid() || !endTime.isValid()) {
			return null;
		} // not valid start or end time -> handle like store is open
		if (pickupTime.isSameOrAfter(startTime) && pickupTime.isSameOrBefore(endTime)) {
			return null
		} // store is open
	}
	return 'Store is closed or will be closing soon! [2]'; // TODO: check message
}

// automatic thumbnail generation.

const Image = require("parse-image");

Parse.Cloud.beforeSave("Category", function(request, response) {
	const parseObject = request.object;
	createThumbnail(parseObject, response);
});

Parse.Cloud.beforeSave("Menu", function(request, response) {
	const parseObject = request.object;
	createThumbnail(parseObject, response);
});

Parse.Cloud.beforeSave("MenuItem", function(request, response) {
	const parseObject = request.object;
	createThumbnail(parseObject, response);
});

function createThumbnail(parseObject, response) {
	if (parseObject.get("image") == null) {
		parseObject.unset("thumbnail");
		response.success();
		return;
	}

	if (parseObject.dirty("image") || parseObject.get("thumbnail") == null) {
		//proceed as planned
	} else {
		//nothing to do!
		response.success();
		return;
	}

	Parse.Cloud.httpRequest({
		url: parseObject.get("image").url()

	}).then(function(response) {
		const image = new Image();
		return image.setData(response.buffer);

	}).then(function(image) {
		// Crop the image to the smaller of width or height.
		const minSize = Math.min(image.width(), image.height());
		if(minSize === image.width())
		{
			return image.scale({
				width: 160,
				height: 160*image.height()/image.width()
			});
		}
		else
		{
			return image.scale({
				width: 160*image.width()/image.height(),
				height: 160
			});
		}
	}).then(function(image) {
		// Make sure it's a PNG to preserve transparency.
		return image.setFormat("PNG");
	}).then(function(image) {
		// Get the image data in a Buffer.
		return image.data();

	}).then(function(buffer) {
		// Save the image into a new file.
		const base64 = buffer.toString("base64");
		const cropped = new Parse.File("thumbnail.png", {base64: base64});
		return cropped.save();

	}).then(function(cropped) {
		// Attach the image file to the original object.
		parseObject.set("thumbnail", cropped);

	}).then(function(result) {
		response.success();
	}, function(error) {
		response.error(error);
	});
}

// END automatic thumbnail generation

function debugLog(anything) {
	console.log(anything);
}

function errorLog(anything) {
	console.error(anything);
}
